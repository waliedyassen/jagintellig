// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.neptune.jagintellij.config.psi.ConfigElementTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.neptune.jagintellij.config.psi.*;

public class ConfigPropertyValueImpl extends ASTWrapperPsiElement implements ConfigPropertyValue {

  public ConfigPropertyValueImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitPropertyValue(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public ConfigLiteralBoolean getLiteralBoolean() {
    return findChildByClass(ConfigLiteralBoolean.class);
  }

  @Override
  @Nullable
  public ConfigLiteralConstant getLiteralConstant() {
    return findChildByClass(ConfigLiteralConstant.class);
  }

  @Override
  @Nullable
  public ConfigLiteralDynamic getLiteralDynamic() {
    return findChildByClass(ConfigLiteralDynamic.class);
  }

  @Override
  @Nullable
  public ConfigLiteralNull getLiteralNull() {
    return findChildByClass(ConfigLiteralNull.class);
  }

  @Override
  @Nullable
  public ConfigLiteralNumber getLiteralNumber() {
    return findChildByClass(ConfigLiteralNumber.class);
  }

  @Override
  @Nullable
  public ConfigLiteralString getLiteralString() {
    return findChildByClass(ConfigLiteralString.class);
  }

}
