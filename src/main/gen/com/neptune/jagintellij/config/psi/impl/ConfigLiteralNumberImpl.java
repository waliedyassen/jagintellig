// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.neptune.jagintellij.config.psi.ConfigElementTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.neptune.jagintellij.config.psi.*;

public class ConfigLiteralNumberImpl extends ASTWrapperPsiElement implements ConfigLiteralNumber {

  public ConfigLiteralNumberImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitLiteralNumber(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public PsiElement getNumber() {
    return findNotNullChildByType(NUMBER);
  }

}
