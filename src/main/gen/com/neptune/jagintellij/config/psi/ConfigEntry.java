// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ConfigEntry extends PsiElement {

  @Nullable
  ConfigConstant getConstant();

  @Nullable
  ConfigType getType();

}
