// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ConfigPropertyValue extends PsiElement {

  @Nullable
  ConfigLiteralBoolean getLiteralBoolean();

  @Nullable
  ConfigLiteralConstant getLiteralConstant();

  @Nullable
  ConfigLiteralDynamic getLiteralDynamic();

  @Nullable
  ConfigLiteralNull getLiteralNull();

  @Nullable
  ConfigLiteralNumber getLiteralNumber();

  @Nullable
  ConfigLiteralString getLiteralString();

}
