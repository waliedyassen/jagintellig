// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import com.neptune.jagintellij.config.psi.impl.*;

public interface ConfigElementTypes {

  IElementType CONSTANT = new ConfigElementType("CONSTANT");
  IElementType CONSTANT_NAME = new ConfigElementType("CONSTANT_NAME");
  IElementType CONSTANT_VALUE = new ConfigElementType("CONSTANT_VALUE");
  IElementType ENTRY = new ConfigElementType("ENTRY");
  IElementType LITERAL_BOOLEAN = new ConfigElementType("LITERAL_BOOLEAN");
  IElementType LITERAL_CONSTANT = new ConfigElementType("LITERAL_CONSTANT");
  IElementType LITERAL_DYNAMIC = new ConfigElementType("LITERAL_DYNAMIC");
  IElementType LITERAL_NULL = new ConfigElementType("LITERAL_NULL");
  IElementType LITERAL_NUMBER = new ConfigElementType("LITERAL_NUMBER");
  IElementType LITERAL_STRING = new ConfigElementType("LITERAL_STRING");
  IElementType PROPERTY = new ConfigElementType("PROPERTY");
  IElementType PROPERTY_KEY = new ConfigElementType("PROPERTY_KEY");
  IElementType PROPERTY_VALUE = new ConfigElementType("PROPERTY_VALUE");
  IElementType TYPE = new ConfigElementType("TYPE");
  IElementType TYPE_NAME = new ConfigElementType("TYPE_NAME");

  IElementType CARET = new ConfigTokenType("^");
  IElementType COMMA = new ConfigTokenType(",");
  IElementType COMMENT = new ConfigTokenType("COMMENT");
  IElementType EQUAL = new ConfigTokenType("=");
  IElementType FALSE = new ConfigTokenType("false");
  IElementType IDENTIFIER = new ConfigTokenType("IDENTIFIER");
  IElementType LBRACKET = new ConfigTokenType("[");
  IElementType NO = new ConfigTokenType("no");
  IElementType NULL = new ConfigTokenType("null");
  IElementType NUMBER = new ConfigTokenType("NUMBER");
  IElementType RBRACKET = new ConfigTokenType("]");
  IElementType STRING = new ConfigTokenType("STRING");
  IElementType TRUE = new ConfigTokenType("true");
  IElementType YES = new ConfigTokenType("yes");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == CONSTANT) {
        return new ConfigConstantImpl(node);
      }
      else if (type == CONSTANT_NAME) {
        return new ConfigConstantNameImpl(node);
      }
      else if (type == CONSTANT_VALUE) {
        return new ConfigConstantValueImpl(node);
      }
      else if (type == ENTRY) {
        return new ConfigEntryImpl(node);
      }
      else if (type == LITERAL_BOOLEAN) {
        return new ConfigLiteralBooleanImpl(node);
      }
      else if (type == LITERAL_CONSTANT) {
        return new ConfigLiteralConstantImpl(node);
      }
      else if (type == LITERAL_DYNAMIC) {
        return new ConfigLiteralDynamicImpl(node);
      }
      else if (type == LITERAL_NULL) {
        return new ConfigLiteralNullImpl(node);
      }
      else if (type == LITERAL_NUMBER) {
        return new ConfigLiteralNumberImpl(node);
      }
      else if (type == LITERAL_STRING) {
        return new ConfigLiteralStringImpl(node);
      }
      else if (type == PROPERTY) {
        return new ConfigPropertyImpl(node);
      }
      else if (type == PROPERTY_KEY) {
        return new ConfigPropertyKeyImpl(node);
      }
      else if (type == PROPERTY_VALUE) {
        return new ConfigPropertyValueImpl(node);
      }
      else if (type == TYPE) {
        return new ConfigTypeImpl(node);
      }
      else if (type == TYPE_NAME) {
        return new ConfigTypeNameImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
