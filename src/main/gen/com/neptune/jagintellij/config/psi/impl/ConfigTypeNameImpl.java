// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static com.neptune.jagintellij.config.psi.ConfigElementTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.neptune.jagintellij.config.psi.*;

public class ConfigTypeNameImpl extends ASTWrapperPsiElement implements ConfigTypeName {

  public ConfigTypeNameImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ConfigVisitor visitor) {
    visitor.visitTypeName(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ConfigVisitor) accept((ConfigVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PsiElement getIdentifier() {
    return findChildByType(IDENTIFIER);
  }

  @Override
  @Nullable
  public PsiElement getNumber() {
    return findChildByType(NUMBER);
  }

}
