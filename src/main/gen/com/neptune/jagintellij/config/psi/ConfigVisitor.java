// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class ConfigVisitor extends PsiElementVisitor {

  public void visitConstant(@NotNull ConfigConstant o) {
    visitPsiElement(o);
  }

  public void visitConstantName(@NotNull ConfigConstantName o) {
    visitPsiElement(o);
  }

  public void visitConstantValue(@NotNull ConfigConstantValue o) {
    visitPsiElement(o);
  }

  public void visitEntry(@NotNull ConfigEntry o) {
    visitPsiElement(o);
  }

  public void visitLiteralBoolean(@NotNull ConfigLiteralBoolean o) {
    visitPsiElement(o);
  }

  public void visitLiteralConstant(@NotNull ConfigLiteralConstant o) {
    visitPsiElement(o);
  }

  public void visitLiteralDynamic(@NotNull ConfigLiteralDynamic o) {
    visitPsiElement(o);
  }

  public void visitLiteralNull(@NotNull ConfigLiteralNull o) {
    visitPsiElement(o);
  }

  public void visitLiteralNumber(@NotNull ConfigLiteralNumber o) {
    visitPsiElement(o);
  }

  public void visitLiteralString(@NotNull ConfigLiteralString o) {
    visitPsiElement(o);
  }

  public void visitProperty(@NotNull ConfigProperty o) {
    visitPsiElement(o);
  }

  public void visitPropertyKey(@NotNull ConfigPropertyKey o) {
    visitPsiElement(o);
  }

  public void visitPropertyValue(@NotNull ConfigPropertyValue o) {
    visitPsiElement(o);
  }

  public void visitType(@NotNull ConfigType o) {
    visitPsiElement(o);
  }

  public void visitTypeName(@NotNull ConfigTypeName o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}
