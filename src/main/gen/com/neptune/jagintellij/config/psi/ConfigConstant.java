// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface ConfigConstant extends PsiElement {

  @NotNull
  ConfigConstantName getConstantName();

  @NotNull
  ConfigConstantValue getConstantValue();

}
