// This is a generated file. Not intended for manual editing.
package com.neptune.jagintellij.config.parser;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static com.neptune.jagintellij.config.psi.ConfigElementTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class ConfigParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return unit(b, l + 1);
  }

  /* ********************************************************** */
  // CARET constantName EQUAL constantValue
  public static boolean constant(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constant")) return false;
    if (!nextTokenIs(b, CARET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, CARET);
    r = r && constantName(b, l + 1);
    r = r && consumeToken(b, EQUAL);
    r = r && constantValue(b, l + 1);
    exit_section_(b, m, CONSTANT, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean constantName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constantName")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, CONSTANT_NAME, r);
    return r;
  }

  /* ********************************************************** */
  // literalString | literalNumber | literalNull
  public static boolean constantValue(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constantValue")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, CONSTANT_VALUE, "<constant value>");
    r = literalString(b, l + 1);
    if (!r) r = literalNumber(b, l + 1);
    if (!r) r = literalNull(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // type | constant
  public static boolean entry(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "entry")) return false;
    if (!nextTokenIs(b, "<entry>", CARET, LBRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ENTRY, "<entry>");
    r = type(b, l + 1);
    if (!r) r = constant(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // TRUE | YES | FALSE | NO
  public static boolean literalBoolean(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalBoolean")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, LITERAL_BOOLEAN, "<literal boolean>");
    r = consumeToken(b, TRUE);
    if (!r) r = consumeToken(b, YES);
    if (!r) r = consumeToken(b, FALSE);
    if (!r) r = consumeToken(b, NO);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // CARET IDENTIFIER
  public static boolean literalConstant(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalConstant")) return false;
    if (!nextTokenIs(b, CARET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, CARET, IDENTIFIER);
    exit_section_(b, m, LITERAL_CONSTANT, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean literalDynamic(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalDynamic")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, LITERAL_DYNAMIC, r);
    return r;
  }

  /* ********************************************************** */
  // NULL
  public static boolean literalNull(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalNull")) return false;
    if (!nextTokenIs(b, NULL)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, NULL);
    exit_section_(b, m, LITERAL_NULL, r);
    return r;
  }

  /* ********************************************************** */
  // NUMBER
  public static boolean literalNumber(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalNumber")) return false;
    if (!nextTokenIs(b, NUMBER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, NUMBER);
    exit_section_(b, m, LITERAL_NUMBER, r);
    return r;
  }

  /* ********************************************************** */
  // STRING
  public static boolean literalString(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalString")) return false;
    if (!nextTokenIs(b, STRING)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, STRING);
    exit_section_(b, m, LITERAL_STRING, r);
    return r;
  }

  /* ********************************************************** */
  // propertyKey EQUAL propertyValue (COMMA propertyValue)*
  public static boolean property(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = propertyKey(b, l + 1);
    r = r && consumeToken(b, EQUAL);
    r = r && propertyValue(b, l + 1);
    r = r && property_3(b, l + 1);
    exit_section_(b, m, PROPERTY, r);
    return r;
  }

  // (COMMA propertyValue)*
  private static boolean property_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!property_3_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "property_3", c)) break;
    }
    return true;
  }

  // COMMA propertyValue
  private static boolean property_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "property_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, COMMA);
    r = r && propertyValue(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // IDENTIFIER
  public static boolean propertyKey(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "propertyKey")) return false;
    if (!nextTokenIs(b, IDENTIFIER)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, IDENTIFIER);
    exit_section_(b, m, PROPERTY_KEY, r);
    return r;
  }

  /* ********************************************************** */
  // literalDynamic | literalConstant | literalString | literalNumber | literalBoolean | literalNull
  public static boolean propertyValue(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "propertyValue")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, PROPERTY_VALUE, "<property value>");
    r = literalDynamic(b, l + 1);
    if (!r) r = literalConstant(b, l + 1);
    if (!r) r = literalString(b, l + 1);
    if (!r) r = literalNumber(b, l + 1);
    if (!r) r = literalBoolean(b, l + 1);
    if (!r) r = literalNull(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // LBRACKET typeName RBRACKET property*
  public static boolean type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type")) return false;
    if (!nextTokenIs(b, LBRACKET)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, LBRACKET);
    r = r && typeName(b, l + 1);
    r = r && consumeToken(b, RBRACKET);
    r = r && type_3(b, l + 1);
    exit_section_(b, m, TYPE, r);
    return r;
  }

  // property*
  private static boolean type_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "type_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!property(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "type_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // IDENTIFIER | NUMBER
  public static boolean typeName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typeName")) return false;
    if (!nextTokenIs(b, "<type name>", IDENTIFIER, NUMBER)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, TYPE_NAME, "<type name>");
    r = consumeToken(b, IDENTIFIER);
    if (!r) r = consumeToken(b, NUMBER);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // entry*
  static boolean unit(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "unit")) return false;
    while (true) {
      int c = current_position_(b);
      if (!entry(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "unit", c)) break;
    }
    return true;
  }

}
