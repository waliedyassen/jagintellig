package com.neptune.jagintellij;

import com.intellij.openapi.util.IconLoader

var CONFIG_ICON = IconLoader.getIcon("/com/neptune/jagintellij/config.svg")
var CS2_ICON = IconLoader.getIcon("/com/neptune/jagintellij/cs2.svg")
