package com.neptune.jagintellij.config.brace

import com.intellij.lang.BracePair
import com.intellij.lang.PairedBraceMatcher
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IElementType
import com.neptune.jagintellij.config.psi.ConfigElementTypes


/**
 * A [PairedBraceMatcher] implementation for the RuneScript Configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigBraceMatcher : PairedBraceMatcher {

    override fun getPairs() = PAIRS

    override fun getCodeConstructStart(file: PsiFile?, openingBraceOffset: Int) = openingBraceOffset

    override fun isPairedBracesAllowedBeforeType(lbraceType: IElementType, contextType: IElementType?) = true

    companion object {
        val PAIRS = arrayOf(BracePair(ConfigElementTypes.LBRACKET, ConfigElementTypes.RBRACKET, false))
    }
}
