package com.neptune.jagintellij.config.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.FileViewProvider
import com.neptune.jagintellij.config.lang.ConfigLanguage

/**
 * A RuneScript Configuration language file PSI root.
 *
 * @author Walied K. Yassen
 */
class ConfigPsiFile(private val _fileType: FileType, fileViewProvider: FileViewProvider) : PsiFileBase(fileViewProvider, ConfigLanguage) {
    override fun getFileType(): FileType {
        return _fileType;
    }
}
