package com.neptune.jagintellij.config.psi

import com.intellij.psi.tree.IElementType
import com.neptune.jagintellij.config.lang.ConfigLanguage

/**
 * The base class for all of the RuneScript Configuration language token types.
 *
 * @author Walied K. Yassen
 */
class ConfigTokenType(debugName: String) : IElementType(debugName, ConfigLanguage)
