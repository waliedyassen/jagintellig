package com.neptune.jagintellij.config.validate

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.psi.PsiElement
import com.neptune.jagintellij.config.validate.impl.UnrecognisedPropertyValidation

/**
 * An [Annotator] implementation that is responsible.
 *
 * @author Walied K. Yassen
 */
class ConfigValidationAnnotator : Annotator {

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        val validations = createValidations()
        validations.forEach {
            it.init(holder)
            element.accept(it)
        }
    }

    companion object {
        /**
         * Creates an array of all the available [ConfigValidation] objects.
         *
         * @return an [Array] of all the available [ConfigValidation] objects.
         */
        private fun createValidations() = arrayOf<ConfigValidation>(
            UnrecognisedPropertyValidation()
        )
    }
}
