package com.neptune.jagintellij.config.validate

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.psi.PsiElement
import com.neptune.jagintellij.config.psi.ConfigVisitor

/**
 * The base class for all of our configuration validations.
 *
 * @author Walied K. Yassen
 */
open class ConfigValidation : ConfigVisitor() {

    /**
     * The annotation holder for the validation.
     */
    private lateinit var holder: AnnotationHolder

    /**
     * Initialises this [ConfigValidation] instance.
     *
     * @param holder the annotation holder for the validation.
     */
    fun init(holder: AnnotationHolder) {
        this.holder = holder
    }

    /**
     * Reports a new error to the annotation holder.
     *
     * @param offender the offending element of the error.
     * @param message the message of the error.
     */
    fun reportError(offender: PsiElement, message: String) {
        holder.createErrorAnnotation(offender, message)
    }
}
