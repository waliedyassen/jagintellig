package com.neptune.jagintellij.config.validate.impl

import com.neptune.jagintellij.config.psi.ConfigPropertyKey
import com.neptune.jagintellij.config.validate.ConfigValidation

/**
 * A validation for whether or not a specific property key name is a valid key name.
 *
 * @author Walied K. Yassen
 */
class UnrecognisedPropertyValidation : ConfigValidation() {

    override fun visitPropertyKey(o: ConfigPropertyKey) {
        // TODO:
    }
}
