package com.neptune.jagintellij.config.commenter

import com.intellij.lang.Commenter

/**
 * A [Commenter] implementation for the RuneScript Configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigCommenter : Commenter {

    override fun getCommentedBlockCommentPrefix(): String? = null

    override fun getCommentedBlockCommentSuffix(): String? = null

    override fun getBlockCommentPrefix(): String? = null

    override fun getBlockCommentSuffix(): String? = null

    override fun getLineCommentPrefix(): String? = "//"
}
