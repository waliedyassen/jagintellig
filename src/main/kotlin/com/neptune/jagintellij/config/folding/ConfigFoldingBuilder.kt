package com.neptune.jagintellij.config.folding

import com.intellij.lang.ASTNode
import com.intellij.lang.folding.FoldingBuilderEx
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiRecursiveElementVisitor
import com.neptune.jagintellij.config.psi.ConfigElementTypes
import com.neptune.jagintellij.config.psi.ConfigType

/**
 * A [FoldingBuilderEx] implementation for the RuneScript Configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigFoldingBuilder : FoldingBuilderEx(), DumbAware {

    override fun buildFoldRegions(root: PsiElement, document: Document, quick: Boolean): Array<FoldingDescriptor> {
        if (quick) {
            return emptyArray()
        }
        val descriptors = mutableListOf<FoldingDescriptor>()
        root.accept(object : PsiRecursiveElementVisitor() {
            override fun visitElement(element: PsiElement?) {
                when (element) {
                    is ConfigType -> {
                        val properties = element.propertyList
                        if (properties.size > 1) {
                            val start = properties.first().textRange.startOffset
                            val end = properties.last().textRange.endOffset
                            val range = TextRange(start, end)
                            if (range.length > 0) {
                                descriptors.add(FoldingDescriptor(element, range))
                            }
                        }
                    }
                }
                super.visitElement(element)
            }
        })
        return descriptors.toTypedArray()
    }

    override fun getPlaceholderText(node: ASTNode): String? {
        return when (node.elementType) {
            ConfigElementTypes.ENTRY -> "Properties Count: ${(node.psi as ConfigType).propertyList.size}"
            else -> "..."
        }
    }

    override fun isCollapsedByDefault(node: ASTNode) = false
}
