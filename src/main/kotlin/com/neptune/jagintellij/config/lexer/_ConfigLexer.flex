package com.neptune.jagintellij.config.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static com.neptune.jagintellij.config.psi.ConfigElementTypes.*;

%%

%{
  public _ConfigLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class _ConfigLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

NUMBER=[-+]?[0-9]+
IDENTIFIER=[a-zA-Z0-9_]+
STRING=\"([^\"\\]|\\\"|\\)*\"
COMMENT="//"[^\r\n]*

%%
<YYINITIAL> {
  {WHITE_SPACE}      { return WHITE_SPACE; }

  "["                { return LBRACKET; }
  "]"                { return RBRACKET; }
  ","                { return COMMA; }
  "="                { return EQUAL; }
  "^"                { return CARET; }
  "yes"              { return YES; }
  "no"               { return NO; }
  "true"             { return TRUE; }
  "false"            { return FALSE; }
  "null"             { return NULL; }

  {NUMBER}           { return NUMBER; }
  {IDENTIFIER}       { return IDENTIFIER; }
  {STRING}           { return STRING; }
  {COMMENT}          { return COMMENT; }

}

[^] { return BAD_CHARACTER; }
