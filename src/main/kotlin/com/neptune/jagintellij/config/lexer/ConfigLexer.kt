package com.neptune.jagintellij.config.lexer

import com.intellij.lexer.FlexAdapter

/**
 * A wrapper for the [_ConfigLexer] auto-generated class.
 *
 * @author Walied K. Yassen
 */
class ConfigLexer : FlexAdapter(_ConfigLexer())
