package com.neptune.jagintellij.config.annotator

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.psi.PsiElement
import com.neptune.jagintellij.config.highlighter.ConfigHighlighter
import com.neptune.jagintellij.config.psi.*

/**
 * An [Annotator] implementation that highlights RuneScript Configuration language [PsiElement]s.
 *
 * @author Walied K. Yassen
 */
class ConfigAnnotator : Annotator {

    override fun annotate(element: PsiElement, holder: AnnotationHolder) {
        when (element) {
            is ConfigTypeName -> holder.createInfoAnnotation(element, "Configuration ${element.text}").textAttributes = ConfigHighlighter.ENTRY
            is ConfigPropertyKey -> holder.createInfoAnnotation(element, null).textAttributes = ConfigHighlighter.KEY
            is ConfigLiteralConstant -> holder.createInfoAnnotation(element, null).textAttributes = ConfigHighlighter.CONSTANT
            is ConfigLiteralDynamic -> holder.createInfoAnnotation(element, null).textAttributes = ConfigHighlighter.DYNAMIC
            is ConfigConstantName -> holder.createInfoAnnotation(element, "Constant ${element.text}").textAttributes = ConfigHighlighter.CONSTANT
        }
    }
}
