package com.neptune.jagintellij.config.lang

import com.intellij.lang.Language

/**
 * The RuneScript Configuration [Language] implementation.
 *
 * @author Walied K. Yassen
 */
object ConfigLanguage : Language("RuneScriptConfig")
