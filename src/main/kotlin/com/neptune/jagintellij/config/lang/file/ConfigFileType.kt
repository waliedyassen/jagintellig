package com.neptune.jagintellij.config.lang.file

import com.intellij.openapi.fileTypes.LanguageFileType
import com.neptune.jagintellij.CONFIG_ICON
import com.neptune.jagintellij.config.lang.ConfigLanguage
import javax.swing.Icon

/**
 * A [LanguageFileType] implementation for the RuneScript Configuration files.
 *
 * @author Walied K. Yassen
 */
class ConfigFileType(private val configName: String, private val configExtension: String) : LanguageFileType(ConfigLanguage) {

    override fun getName() = "$configName"

    override fun getDescription() = "RuneScript $configName Configuration"

    override fun getDefaultExtension() = configExtension

    override fun getIcon(): Icon? = CONFIG_ICON
}
