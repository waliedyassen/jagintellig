package com.neptune.jagintellij.config.lang.file

import com.intellij.openapi.fileTypes.FileTypeConsumer
import com.intellij.openapi.fileTypes.FileTypeFactory

/**
 * A [FileTypeFactory] implementation that registers all of the [RECOGNISED_EXTENSIONS](Recognised Extensions) of the
 * RuneScript Configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigFileTypeFactory : FileTypeFactory() {

    override fun createFileTypes(consumer: FileTypeConsumer) {
        RECOGNISED_EXTENSIONS.forEach {
            // TODO Add the associated configuration name with the extension.
            consumer.consume(ConfigFileType(it, it))
        }
    }

    companion object {
        /**
         * A set of all the recognised extensions by the language.
         */
        val RECOGNISED_EXTENSIONS =
            setOf(  "flu", "flo", "idk", "inv", "loc", "enum", "npc", "obj", "param", "struct", "seq", "spot", "area", "varp",
                    "varclient", "varbit", "hitmark", "headbar", "mapelement", "mesanim", "cat", "controller", "gamelogevent"
            )
    }
}
