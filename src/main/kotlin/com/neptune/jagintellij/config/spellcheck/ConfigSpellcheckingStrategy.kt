package com.neptune.jagintellij.config.spellcheck

import com.intellij.psi.PsiElement
import com.intellij.spellchecker.tokenizer.SpellcheckingStrategy
import com.intellij.spellchecker.tokenizer.Tokenizer
import com.neptune.jagintellij.config.psi.ConfigElementTypes

/**
 * A [SpellcheckingStrategy] implementation for the RuneScript Configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigSpellcheckingStrategy : SpellcheckingStrategy() {

    override fun getTokenizer(element: PsiElement?): Tokenizer<*> {
        return when (element!!.node.elementType) {
            ConfigElementTypes.LITERAL_STRING -> TEXT_TOKENIZER
            ConfigElementTypes.COMMENT -> TEXT_TOKENIZER
            else -> EMPTY_TOKENIZER
        }
    }
}
