package com.neptune.jagintellij.config.parser

import com.intellij.lang.ASTNode
import com.intellij.lang.ParserDefinition
import com.intellij.openapi.project.Project
import com.intellij.psi.FileViewProvider
import com.intellij.psi.tree.IFileElementType
import com.intellij.psi.tree.TokenSet
import com.neptune.jagintellij.config.lang.ConfigLanguage
import com.neptune.jagintellij.config.lexer.ConfigLexer
import com.neptune.jagintellij.config.psi.ConfigElementTypes
import com.neptune.jagintellij.config.psi.ConfigPsiFile

/**
 * The RuneScript Configuration language parser definition.
 *
 * @author Walied K. Yassen
 */
class ConfigParserDefinition : ParserDefinition {

    /**
     * The configuration file node type.
     */
    val FILE_NODE_TYPE = IFileElementType(ConfigLanguage)

    override fun createParser(project: Project?) = ConfigParser()

    override fun createFile(viewProvider: FileViewProvider?) = ConfigPsiFile(viewProvider!!.fileType, viewProvider);

    override fun getStringLiteralElements() = TokenSet.EMPTY

    override fun getFileNodeType() = FILE_NODE_TYPE

    override fun createLexer(project: Project?) = ConfigLexer()

    override fun createElement(node: ASTNode?) = ConfigElementTypes.Factory.createElement(node)

    override fun getCommentTokens() = COMMENT_TOKENS

    companion object {
        val COMMENT_TOKENS = TokenSet.create(ConfigElementTypes.COMMENT)
    }
}
