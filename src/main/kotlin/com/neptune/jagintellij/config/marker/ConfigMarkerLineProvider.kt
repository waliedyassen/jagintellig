package com.neptune.jagintellij.config.marker

import com.intellij.codeInsight.daemon.LineMarkerInfo
import com.intellij.codeInsight.daemon.LineMarkerProvider
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder
import com.intellij.psi.PsiElement
import com.neptune.jagintellij.CONFIG_ICON
import com.neptune.jagintellij.config.psi.ConfigConstant
import com.neptune.jagintellij.config.psi.ConfigEntry
import com.neptune.jagintellij.config.psi.ConfigType
import com.neptune.jagintellij.config.psi.ConfigTypeName

/**
 * A [LineMarkerProvider] implementation for the RuneScript configuration language.
 *
 * @author Walied K. Yassen
 */
class ConfigMarkerLineProvider : LineMarkerProvider {

    override fun getLineMarkerInfo(element: PsiElement): LineMarkerInfo<*>? {
        return when (element) {
            is ConfigType -> NavigationGutterIconBuilder.create(CONFIG_ICON).setTargets(element).setTooltipText("Navigate to configuration ${element.typeName.text}").createLineMarkerInfo(element)
            is ConfigConstant -> NavigationGutterIconBuilder.create(CONFIG_ICON).setTargets(element).setTooltipText("Navigate to constant ${element.constantName.text}").createLineMarkerInfo(element)
            else -> null
        }
    }
}
