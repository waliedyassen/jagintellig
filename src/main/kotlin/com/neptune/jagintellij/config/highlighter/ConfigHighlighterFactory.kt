package com.neptune.jagintellij.config.highlighter

import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile

/**
 * A [SyntaxHighlighterFactory] implementation that produces a [ConfigHighlighter] object.
 *
 * @author Walied K. Yassen
 */
class ConfigHighlighterFactory : SyntaxHighlighterFactory() {

    override fun getSyntaxHighlighter(project: Project?, virtualFile: VirtualFile?) = ConfigHighlighter()
}
