package com.neptune.jagintellij.config.highlighter

import com.intellij.execution.process.ConsoleHighlighter
import com.intellij.execution.ui.ConsoleViewContentType
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors
import com.intellij.openapi.editor.colors.CodeInsightColors
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.tree.IElementType
import com.neptune.jagintellij.config.lexer.ConfigLexer
import com.neptune.jagintellij.config.psi.ConfigElementTypes


/**
 * A [SyntaxHighlighterBase] implementation that highlights all of the RuneScript Configuration syntax.
 *
 * @author Walied K. Yassen
 */
class ConfigHighlighter : SyntaxHighlighterBase() {

    override fun getTokenHighlights(tokenType: IElementType?): Array<TextAttributesKey> {
        return pack(when (tokenType) {
            ConfigElementTypes.LBRACKET, ConfigElementTypes.RBRACKET -> BRACKET
            ConfigElementTypes.YES, ConfigElementTypes.NO -> KEYWORD
            ConfigElementTypes.TRUE, ConfigElementTypes.FALSE -> KEYWORD
            ConfigElementTypes.NULL -> KEYWORD
            ConfigElementTypes.STRING -> STRING
            ConfigElementTypes.NUMBER -> NUMBER
            ConfigElementTypes.COMMENT -> COMMENT
            else -> null
        })
    }

    override fun getHighlightingLexer() = ConfigLexer()

    companion object {
        val BRACKET = TextAttributesKey.createTextAttributesKey("runescript.config.bracket", DefaultLanguageHighlighterColors.BRACKETS)
        val ENTRY = TextAttributesKey.createTextAttributesKey("runescript.config.entry", DefaultLanguageHighlighterColors.KEYWORD)
        val KEY = TextAttributesKey.createTextAttributesKey("runescript.config.key", DefaultLanguageHighlighterColors.INSTANCE_METHOD)
        val KEYWORD = TextAttributesKey.createTextAttributesKey("runescript.config.keyword", ConsoleHighlighter.BLUE)
        val CONSTANT = TextAttributesKey.createTextAttributesKey("runescript.config.constant", ConsoleHighlighter.CYAN)
        val DYNAMIC = TextAttributesKey.createTextAttributesKey("runescript.config.dynamic", DefaultLanguageHighlighterColors.INSTANCE_FIELD)
        val STRING = TextAttributesKey.createTextAttributesKey("runescript.config.string", DefaultLanguageHighlighterColors.STRING)
        val NUMBER = TextAttributesKey.createTextAttributesKey("runescript.config.number", DefaultLanguageHighlighterColors.NUMBER)
        val COMMENT = TextAttributesKey.createTextAttributesKey("runescript.config.comment", DefaultLanguageHighlighterColors.BLOCK_COMMENT)
    }
}
