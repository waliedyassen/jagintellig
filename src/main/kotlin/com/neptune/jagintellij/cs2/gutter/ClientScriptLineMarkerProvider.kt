package com.neptune.jagintellij.cs2.gutter

import com.intellij.codeInsight.daemon.LineMarkerInfo
import com.intellij.codeInsight.daemon.LineMarkerProvider
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder
import com.intellij.psi.PsiElement
import com.neptune.jagintellij.CS2_ICON
import com.neptune.jagintellij.cs2.ClientScriptParserDefinition
import com.neptune.jagintellij.parser.ClientScriptParser

/**
 * The gutter lines icons provider for the ClientScript language.
 *
 * @author Walied K. Yassen
 */
class ClientScriptLineMarkerProvider : LineMarkerProvider {

    override fun getLineMarkerInfo(element: PsiElement): LineMarkerInfo<*>? {
        if (element.node.elementType == ClientScriptParserDefinition.rules[ClientScriptParser.RULE_scriptName]) {
            val builder = NavigationGutterIconBuilder.create(CS2_ICON).setTargets(element).setTooltipText("Navigate to " + element.node.text)
            return builder.createLineMarkerInfo(element)
        }
        return null
    }
}
